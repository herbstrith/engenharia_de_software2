package br.ufrgs.inf.engii.restaurante;

public enum Estado {
	PENDENTE, EM_PREPARACAO, PRONTO;
}

package br.ufrgs.inf.engii.restaurante;

public abstract class Funcionario {
	private String ID;
	private Double valorFixo;
	public Funcionario(){
		this.valorFixo = 0.0;
	}
	public String getID(){
		return this.ID;
	}
	public void setID(String newID){
		this.ID = newID;
	}
	public Double getValorFixo(){
		return this.valorFixo;
	}
	public void setValorFixo(Double newValor){
		valorFixo = newValor;
	}
}

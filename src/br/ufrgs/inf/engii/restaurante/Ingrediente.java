package br.ufrgs.inf.engii.restaurante;

public class Ingrediente {
	
	private String nome;
	
	public String getNome(){
		return this.nome;
	}
	public void setNome(String newNome){
		this.nome = newNome;
	}
	
}

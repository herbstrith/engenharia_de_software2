package br.ufrgs.inf.engii.restaurante;

import java.util.Map;

public class Item {
	private Double preco;
	private String categoria;
	private Double custo;
	private Double tempo;
	private Receita receita;

	//TODO class constructor
	public Item(Double setPreco, String setCategoria, Double setCusto, Double setTempo, Receita setReceita) {
		preco = setPreco;
		categoria = setCategoria;
		custo = setCusto;
		tempo = setTempo;
		receita = setReceita;
	}
	public Double getPreco(){
		return this.preco;
	}
	public void setPreco(Double newPreco){
		this.preco = newPreco;
	}
	public Map<Ingrediente,Double> getIngredientes(){
		return this.receita.getIngredientes();
	}
	public Double getCusto(){
		return this.custo;
	}
	public void setCusto(Double newCusto){
		this.custo = newCusto;
	}
	public Double getTempo(){
		return this.tempo;
	}
	public void setTempo(Double newTempo){
		this.tempo = newTempo;
	}
	public Receita getReceita(){
		return this.receita;
	}
	public void setReceita(Receita newReceita){
		this.receita = newReceita;
	}
	public String getCategoria(){
		return this.categoria;
	}
	public void setCategoria(String newCategoria){
		this.categoria = newCategoria;
	}
}

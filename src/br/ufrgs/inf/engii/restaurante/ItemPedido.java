package br.ufrgs.inf.engii.restaurante;

public class ItemPedido {
	private Item item;
	private Estado estado;

	public ItemPedido(Estado setEstado, Item setItem) {
		estado = setEstado;
		item = setItem;
	}

	public ItemPedido(){};
	
	public Item getItem(){
		return this.item;
	}
	public Estado getEstado(){
		return this.estado;
	}
	public void setItem(Item newItem){
		this.item = newItem;
	}
	public void setEstado(Estado newEstado){
		this.estado = newEstado;
	}
}

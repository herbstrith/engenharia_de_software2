package br.ufrgs.inf.engii.restaurante;

import java.util.Date;
import java.util.Timer;

import br.ufrgs.inf.engii.restaurante.exceptions.AcaoInvalidaException;

public class Mesa {
	private Setor setor;
	private int capacidade;
	private Boolean limpa;
	private Boolean ocupada;
	private Date reserva;
	private Pedido pedido;
	private Double gorjetaLimpeza;
	
	public Mesa(){
		this.setor = new Setor();
		this.gorjetaLimpeza = 0.0;
	}
	public Mesa(Setor setSetor, int setCapacidade, Boolean setLimpa, Boolean setOcupada,
				Date setReserva, Pedido setPedido, Double setGorjeta) {
		setor = setSetor;
		capacidade = setCapacidade;
		limpa = setLimpa;
		ocupada = setOcupada;
		reserva = setReserva;
		pedido = setPedido;
		gorjetaLimpeza = setGorjeta;

	}
	public Setor getSetor() {
		return setor;
	}
	public int getCapacidade() {
		return capacidade;
	}
	public void setCapacidade(int capacidade) throws AcaoInvalidaException {
		if(capacidade > 0)
			this.capacidade = capacidade;
		else
			throw new AcaoInvalidaException();
	}
	public Boolean isLimpa(){
		return this.limpa;
	}
	public Boolean isOcupada(){
		return this.ocupada;
	}
	public void ocupa() throws AcaoInvalidaException{
		if(limpa)
			this.ocupada = true;
		else
			throw new AcaoInvalidaException();
	}
	public void desocupa(){
		this.ocupada = false;
		this.reserva = null;
		this.pedido = null;
		this.limpa = false;
	}
	public void setReserva(Date horario){
		this.reserva = horario;
	}
	public Date getReserva(){
		return this.reserva;
	}
	public Boolean isDisponivel(){
		return (limpa && !ocupada && (reserva == null));
	}
	public void setPedido(Pedido newPedido){
		pedido = newPedido;
	}
	public Pedido getPedido(){
		return pedido;
	}
	public void setGorjetaLimpeza(Double valor){
		gorjetaLimpeza = valor;
		limpa = true;
	}
	public Double getGorjetaLimpeza(){
		return gorjetaLimpeza;
	}
	
	
	
	
	
}

package br.ufrgs.inf.engii.restaurante;

import java.util.ArrayList;
import java.util.List;

public class Pedido {
	private Mesa mesa;
	private List<ItemPedido> itens;
	private Estado estado;
	private Garcom garcom;
	
	public Pedido(Garcom newGarsom, List<Item> newItens){
		this.itens = new ArrayList<ItemPedido>();
		this.garcom = newGarsom;
		this.estado = Estado.PENDENTE;
		for (Item item : newItens) {
			ItemPedido itemPedido = new ItemPedido();
			itemPedido.setItem(item);
			itemPedido.setEstado(Estado.PENDENTE);	
			this.itens.add(itemPedido);
		}
	}
	public Estado getEstado(){
		return this.estado;
	}
	public void setEstado(Estado newEstado){
		this.estado = newEstado;
	}
	public void setEstadoItens(List<Item> newItens, Estado newEstado){
		for (Item item : newItens) {
			ItemPedido itemPedido = new ItemPedido();
			itemPedido.setItem(item);
			itemPedido.setEstado(newEstado);	
			this.itens.add(itemPedido);
		}
	}
	public Garcom getGarcom(){
		return this.garcom;
	}
	public void setGarcom(Garcom newGarcom){
		this.garcom = newGarcom;
	}
	public Double getPreco(){
		Double precoTotal = 0.0;
		for (ItemPedido itemPedido : itens) {
			precoTotal += itemPedido.getItem().getPreco();
		}
		return precoTotal;
	}
	public Double getCusto(){
		Double custoTotal = 0.0;
		for (ItemPedido itemPedido : itens) {
			custoTotal += itemPedido.getItem().getCusto();
		}
		return custoTotal;
	}
	
	public Double getTaxaServico(){
		return this.getPreco()*0.3;
	}
	
	public void addItems(Pedido pedido){
		this.itens.addAll(pedido.itens);
	}
	public void addItem(Item item){
		ItemPedido itemPedido = new ItemPedido();
		itemPedido.setItem(item);
		itemPedido.setEstado(Estado.PENDENTE);	
		this.itens.add(itemPedido);
	}
	public List<ItemPedido> getItens(){
		return this.itens;
	}
	public Mesa getMesa(){
		return this.mesa;
	}
	public void setMesa(Mesa newMesa){
		this.mesa = newMesa;
	}
}

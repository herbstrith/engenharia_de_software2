package br.ufrgs.inf.engii.restaurante;

import java.util.HashMap;
import java.util.Map;

public class Receita {
	private String instrucoes;
	private Map<Ingrediente,Double> ingredientes;
	public Receita(){
		this.ingredientes = new HashMap<Ingrediente, Double>();
	}

	public Receita(String instrucoes_receita, Map<Ingrediente, Double> ingredientes_receita){
		this.instrucoes = instrucoes_receita;
		this.ingredientes = new HashMap<Ingrediente, Double>();
		this.ingredientes = ingredientes_receita;
	}

	public String getInstrucoes(){
		return this.instrucoes;
	}
	public void setInstrucoes(String newInstrucao){
		this.instrucoes = newInstrucao;
	}
	public Map<Ingrediente,Double> getIngredientes(){
		return this.ingredientes;
	}
	public void setIngredientes(Map<Ingrediente,Double> newIngredientes){
		this.ingredientes = newIngredientes;
	}

}

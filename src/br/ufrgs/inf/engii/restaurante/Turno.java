package br.ufrgs.inf.engii.restaurante;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import br.ufrgs.inf.engii.restaurante.exceptions.PedidoEmAbertoException;
import br.ufrgs.inf.engii.restaurante.exceptions.PedidoFechadoException;
import br.ufrgs.inf.engii.restaurante.exceptions.PedidoInexistenteException;

public class Turno {
	private Double custo;
	private Double lucro;
	private Map<Garcom, Setor> garcomSetor;
	private List<Pedido> pedidos;
	private Map<Funcionario, Double> gorjetas;
	
	public Turno(){
		this.garcomSetor = new HashMap<Garcom,Setor>();
		this.gorjetas = new HashMap<Funcionario, Double>();
		this.pedidos = new ArrayList<Pedido>();
		this.custo = 0.0;
		this.lucro = 0.0;
		this.gorjetas.put(new AuxiliarCozinha(), 0.0);
		this.gorjetas.put(new Cozinheiro(), 0.0);
		this.gorjetas.put(new Gerente(), 0.0);
		this.gorjetas.put(new Atendente(), 0.0);
	}
	
	public void addPedido(Pedido newPedido){
		this.pedidos.add(newPedido);
	}
	public List<Pedido> getPedidos(){
		return this.pedidos;
	}
	private void addCusto(Double newCusto){
		this.custo += newCusto;
	}
	private void addLucro(Double newLucro){
		this.lucro += newLucro;
	}
	public Setor getSetor(Garcom oldGarcom){
		return this.garcomSetor.get(oldGarcom);
	}
	public void addGorjeta(Funcionario funcionario, Double valor){
		Double saldo = this.gorjetas.get(funcionario);
		if(saldo == null) saldo = 0.0;
		this.gorjetas.put(funcionario, saldo+valor);
	}
	public List<Garcom> getGarcons(){
		List<Garcom> garcons = new ArrayList<Garcom>();
		for(Garcom g : this.garcomSetor.keySet()){
			if(!garcons.contains(g))
				garcons.add(g);
		}
		return garcons;
	}
	public void setGarconsSetor(Map<List<Garcom>,Setor> garconsSetor){
		for (List<Garcom> garcons : garconsSetor.keySet()) {
			for (Garcom garcom : garcons) {
				this.garcomSetor.put(garcom, garconsSetor.get(garcons));
			}
		}
	}
	public Double getSomaLucro(){
		return this.lucro;
	}
	public Double getSomaCustos(){
		return this.custo;
	}
	public void alterarEstadoItensFinalizado(Pedido pedido)  throws PedidoInexistenteException {
		if(pedidos.contains(pedido)){
			for (ItemPedido itemPedido : pedido.getItens()) {
				itemPedido.setEstado(Estado.PRONTO);
			}			
		}
		else throw new PedidoInexistenteException();	
	}
	public Boolean todosItensFinalizado(Pedido pedido) throws PedidoInexistenteException {
		if(pedidos.contains(pedido)){
			if (pedido.getEstado() == Estado.PRONTO) return true;
			for (ItemPedido itemPedido : pedido.getItens()) {
				if(!itemPedido.getEstado().equals(Estado.PRONTO)) return false; 
			}
			return true;
		}
		else throw new PedidoInexistenteException();	
	}
	public void finalizarPedido(Pedido pedido, Boolean incluiTaxaServico) throws PedidoFechadoException {
		if(pedido.getEstado() == Estado.PRONTO) throw new PedidoFechadoException();
		Double totalConta = pedido.getPreco();
		Double totalCusto = pedido.getCusto();		
		Double taxaServico = pedido.getTaxaServico();		
		if(incluiTaxaServico){
			this.addGorjeta(pedido.getGarcom(), taxaServico*0.7);
			pedido.getMesa().setGorjetaLimpeza(taxaServico*0.3);
		}else{
			totalCusto += taxaServico;
		}
		this.addLucro(totalConta-totalCusto);
		this.addCusto(totalCusto);
		pedido.getMesa().desocupa();
		pedido.setEstado(Estado.PRONTO);
	}
	public Map<Funcionario, Double> getFolhaPgto(){
		Map<Funcionario, Double> folha = new HashMap<Funcionario, Double>();
		for (Funcionario f : gorjetas.keySet()) {
			if(f instanceof Atendente) folha.put(f, this.lucro * 0.01);
			if(f instanceof Cozinheiro) folha.put(f, this.lucro * 0.05);
			if(f instanceof Gerente) folha.put(f, this.lucro * 0.03);
			if(f instanceof Garcom) folha.put(f, gorjetas.get(f));
			if(f instanceof AuxiliarCozinha) folha.put(f, gorjetas.get(f));
			folha.put(f, folha.get(f) + f.getValorFixo());
		}
		return folha;
	}
}

package br.ufrgs.inf.engii.restaurante.exceptions;

public class AcaoInvalidaException extends Exception {
	private static final long serialVersionUID = 2L;
	private String message;
	
	@Override
	public String getMessage() {
		return this.message;		
	};

}

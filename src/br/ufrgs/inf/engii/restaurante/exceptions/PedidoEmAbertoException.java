package br.ufrgs.inf.engii.restaurante.exceptions;

public class PedidoEmAbertoException extends Exception {
	private static final long serialVersionUID = 2L;
	
	@Override
	public String getMessage() {
		return "Existe um pedido com itens n�o prontos";		
	};
}


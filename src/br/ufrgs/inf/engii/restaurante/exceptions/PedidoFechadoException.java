package br.ufrgs.inf.engii.restaurante.exceptions;

public class PedidoFechadoException extends Exception{
	private static final long serialVersionUID = 2L;
	
	@Override
	public String getMessage() {
		return "O pedido j� foi fechado.";		
	};
}

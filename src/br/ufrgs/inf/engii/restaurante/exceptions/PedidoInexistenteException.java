package br.ufrgs.inf.engii.restaurante.exceptions;

public class PedidoInexistenteException extends Exception {
	private static final long serialVersionUID = 2L;
	
	@Override
	public String getMessage() {
		return "O pedido n�o pertence a o turno";		
	};
}
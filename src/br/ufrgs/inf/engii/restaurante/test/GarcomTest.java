package br.ufrgs.inf.engii.restaurante.test;

import static org.junit.Assert.*;

import org.junit.Test;

import br.ufrgs.inf.engii.restaurante.Funcionario;
import br.ufrgs.inf.engii.restaurante.Garcom;

public class GarcomTest {
	private Garcom funcionario;
	@Test
	public void testFuncionario() {
		funcionario = new Garcom();
		assertNotNull(funcionario);
	}

	@Test
	public void testGetSetID() {
		funcionario = new Garcom();
        funcionario.setID("Ciclano");
    	assertEquals(funcionario.getID(), "Ciclano"); 
	}

	@Test
	public void testGetSetValorFixo() {
		funcionario = new Garcom();
		Double valor = new Double(1000.00);
        funcionario.setValorFixo(valor);;
    	assertEquals(funcionario.getValorFixo(),valor);    
	}


}

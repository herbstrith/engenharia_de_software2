package br.ufrgs.inf.engii.restaurante.test;

import br.ufrgs.inf.engii.restaurante.Ingrediente;
import junit.framework.TestCase;

public class IngredienteTest extends TestCase {
    public String teste_get = "banana";
    public String teste_set = "manteiga";
    public Ingrediente ingredienteTeste;


    public void setUp() throws Exception {
        super.setUp();
        ingredienteTeste = new Ingrediente();
        ingredienteTeste.setNome(teste_get);
    }

    public void tearDown() throws Exception {

    }

    public void testGetNome() throws Exception {
        assertEquals(ingredienteTeste.getNome(), teste_get);

    }

    public void testSetNome() throws Exception {
        ingredienteTeste.setNome(teste_set);

        assertEquals(ingredienteTeste.getNome(), teste_set);
    }

}
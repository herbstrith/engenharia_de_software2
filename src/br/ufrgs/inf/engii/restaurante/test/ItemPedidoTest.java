package br.ufrgs.inf.engii.restaurante.test;

import br.ufrgs.inf.engii.restaurante.*;
import junit.framework.TestCase;

import java.util.HashMap;
import java.util.Map;


public class ItemPedidoTest extends TestCase {
    Item itemTeste;
    Estado estadoTeste = Estado.EM_PREPARACAO;

    ItemPedido itempPedidoTeste;
    public void setUp() throws Exception {
        super.setUp();
        Double preco = new Double(10.50f);
        String categoria = "categoria Teste";
        Double custo = new Double(5.00f);
        Double tempo = new Double(340);
        Receita receita;
        Ingrediente ingredienteTeste = new Ingrediente();
        String teste_ingrediente_nome = "banana";
        String instrucoes_teste = "1-asse isso 2-asse aquilo";

        Map<Ingrediente, Double> testeIngredientes = new HashMap();
        ingredienteTeste.setNome(teste_ingrediente_nome);
        testeIngredientes.put(ingredienteTeste, new Double(2.0f));
        receita = new Receita(instrucoes_teste, testeIngredientes );
        itemTeste = new Item(preco, categoria,custo,tempo,receita);
        itempPedidoTeste = new ItemPedido();
        itempPedidoTeste = new ItemPedido(estadoTeste, itemTeste);
    }

    public void tearDown() throws Exception {

    }

    public void testGetItem() throws Exception {
        assertEquals(itempPedidoTeste.getItem(), itemTeste);
    }

    public void testGetEstado() throws Exception {
        assertEquals(itempPedidoTeste.getEstado(), estadoTeste);

    }

    public void testSetItem() throws Exception {
        Double preco2 = new Double(10.50f);
        String categoria2 = "categoria Teste";
        Double custo2 = new Double(5.00f);
        Double tempo2 = new Double(340);
        Receita receita2;
        Ingrediente ingredienteTeste2 = new Ingrediente();
        String teste_ingrediente_nome2 = "banana";
        String instrucoes_teste2 = "1-asse isso 2-asse aquilo";

        Map<Ingrediente, Double> testeIngredientes2 = new HashMap();
        ingredienteTeste2.setNome(teste_ingrediente_nome2);
        testeIngredientes2.put(ingredienteTeste2, new Double(2.0f));
        receita2 = new Receita(instrucoes_teste2, testeIngredientes2 );
        Item itemTeste2 = new Item(preco2, categoria2,custo2,tempo2,receita2);
        itempPedidoTeste.setItem(itemTeste2);
        assertEquals(itempPedidoTeste.getItem(), itemTeste2);

        //test null item
        itempPedidoTeste.setItem(null);
        assertEquals(itempPedidoTeste.getItem(), null);

    }

    public void testSetEstado() throws Exception {
        itempPedidoTeste.setEstado(Estado.PENDENTE);
        assertEquals(itempPedidoTeste.getEstado(), Estado.PENDENTE);

        itempPedidoTeste.setEstado(Estado.EM_PREPARACAO);
        assertEquals(itempPedidoTeste.getEstado(), Estado.EM_PREPARACAO);

        itempPedidoTeste.setEstado(Estado.PRONTO);
        assertEquals(itempPedidoTeste.getEstado(), Estado.PRONTO);

    }

}
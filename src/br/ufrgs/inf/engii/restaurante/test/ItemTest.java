package br.ufrgs.inf.engii.restaurante.test;

import br.ufrgs.inf.engii.restaurante.*;
import junit.framework.TestCase;

import java.util.HashMap;
import java.util.Map;


public class ItemTest extends TestCase {
    Double preco = new Double(10.50f);
    String categoria = "categoria Teste";
    Double custo = new Double(5.00f);
    Double tempo = new Double(340);
    Receita receita;
    Item itemTeste;
    Ingrediente ingredienteTeste = new Ingrediente();
    Map<Ingrediente, Double> testeIngredientes = new HashMap();

    public void setUp() throws Exception {
        super.setUp();

        String teste_ingrediente_nome = "banana";
        String instrucoes_teste = "1-asse isso 2-asse aquilo";

        ingredienteTeste.setNome(teste_ingrediente_nome);
        testeIngredientes.put(ingredienteTeste, new Double(2.0f));
        receita = new Receita(instrucoes_teste, testeIngredientes );
        itemTeste = new Item(preco, categoria,custo,tempo,receita);
    }

    public void tearDown() throws Exception {

    }

    public void testGetPreco() throws Exception {
        assertEquals(itemTeste.getPreco(), preco);
    }

    public void testSetPreco() throws Exception {
        itemTeste.setPreco(new Double(2.49));
        assertEquals(itemTeste.getPreco(), new Double(2.49));
        //test negative
        itemTeste.setPreco(new Double(-2.49));
        assertNotSame(itemTeste.getPreco(), new Double(-2.49));
    }

    public void testGetIngredientes() throws Exception {
        assertEquals(itemTeste.getIngredientes(), testeIngredientes);
    }

    public void testGetCusto() throws Exception {
        assertEquals(itemTeste.getCusto(), custo);

    }

    public void testSetCusto() throws Exception {
        itemTeste.setCusto(new Double(2.49));
        assertEquals(itemTeste.getCusto(), new Double(2.49));
        //test negative
        itemTeste.setCusto(new Double(-2.49));
        assertNotSame(itemTeste.getCusto(), new Double(-2.49));
    }

    public void testGetTempo() throws Exception {
        assertEquals(itemTeste.getTempo(), tempo);

    }

    public void testSetTempo() throws Exception {
        itemTeste.setTempo(new Double(249));
        assertEquals(itemTeste.getTempo(), new Double(249));
        //test negative
        itemTeste.setTempo(new Double(-249));
        assertNotSame(itemTeste.getTempo(), new Double(-249));
    }

    public void testGetReceita() throws Exception {
        assertEquals(itemTeste.getReceita(), receita);

    }

    public void testSetReceita() throws Exception {
        String teste_ingrediente_nome2 = "banana";
        String instrucoes_teste2 = "1-asse isso 2-asse aquilo";

        Map<Ingrediente, Double> testeIngredientes2 = new HashMap();
        ingredienteTeste.setNome(teste_ingrediente_nome2);
        testeIngredientes2.put(ingredienteTeste, new Double(2.0f));
        Receita receita2 = new Receita(instrucoes_teste2, testeIngredientes2 );
        itemTeste.setReceita(receita2);

        assertEquals(itemTeste.getReceita(), receita2);

    }

    public void testGetCategoria() throws Exception {
        assertEquals(itemTeste.getCategoria(), categoria);
    }

    public void testSetCategoria() throws Exception {
        itemTeste.setCategoria("outra categoria");
        assertEquals(itemTeste.getCategoria(), "outra categoria");
        //test empty
        itemTeste.setCategoria("");
        assertEquals(itemTeste.getCategoria(), "");
    }

}
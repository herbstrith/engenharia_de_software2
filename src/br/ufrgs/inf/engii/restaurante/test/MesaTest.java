package br.ufrgs.inf.engii.restaurante.test;

import br.ufrgs.inf.engii.restaurante.*;
import br.ufrgs.inf.engii.restaurante.exceptions.AcaoInvalidaException;
import junit.framework.TestCase;

import java.util.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;


public class MesaTest extends TestCase {
    Setor setorTeste;
    int capacidadeTeste;
    Boolean limpa = true;
    Boolean ocupada;
    Date reserva;
    Pedido pedidoTeste;
    Double gorjetaLimpeza;
    Mesa mesaTeste;
    

    public void setUp() throws Exception {
        super.setUp();

        capacidadeTeste = 4;
        setorTeste = new Setor();
        ocupada = false;
        reserva = new Date();
        gorjetaLimpeza = new Double(2.50);

        List<Item> itensTeste = new ArrayList<Item>();
        Estado estadoTeste = Estado.PENDENTE;
        Garcom garcomTeste = new Garcom();

        // create 2 itens
        Double preco = new Double(10.50f);
        String categoria = "categoria Teste";
        Double custo = new Double(5.00f);
        Double tempo = new Double(340);
        Receita receita;
        Item itemTeste;
        Ingrediente ingredienteTeste = new Ingrediente();
        Map<Ingrediente, Double> testeIngredientes = new HashMap();

        String teste_ingrediente_nome = "banana";
        String instrucoes_teste = "1-asse isso 2-asse aquilo";

        ingredienteTeste.setNome(teste_ingrediente_nome);
        testeIngredientes.put(ingredienteTeste, new Double(2.0f));
        receita = new Receita(instrucoes_teste, testeIngredientes );
        itemTeste = new Item(preco, categoria,custo,tempo,receita);
        teste_ingrediente_nome = "Canela";
        instrucoes_teste = "1-asse isso 2-asse aquilo 2.5 nao esqueca disso";

        ingredienteTeste.setNome(teste_ingrediente_nome);
        testeIngredientes.put(ingredienteTeste, new Double(2.0f));
        receita = new Receita(instrucoes_teste, testeIngredientes );
        Item itemTeste2 = new Item(preco, categoria,custo,tempo,receita);

        itensTeste.add(itemTeste);
        itensTeste.add(itemTeste2);

        pedidoTeste = new Pedido(garcomTeste, itensTeste);
        mesaTeste = new Mesa(setorTeste, capacidadeTeste, limpa, ocupada, reserva, pedidoTeste, gorjetaLimpeza);

    }

    public void tearDown() throws Exception {

    }

    public void testGetSetor() throws Exception {
        assertEquals(mesaTeste.getSetor(), setorTeste);
    }

    public void testGetCapacidade() throws Exception {
        assertEquals(mesaTeste.getCapacidade(), capacidadeTeste);
    }

    public void testSetCapacidade(){
    	try{
	    	mesaTeste.setCapacidade(2);
	        assertEquals(mesaTeste.getCapacidade(), 2);
    	}
    	catch(AcaoInvalidaException unexpectedException){
    		fail("Retornou exception onde n�o deveria");
    	}
    	try{
	    	mesaTeste.setCapacidade(-2);        
	        fail("N�o deu exception!");
        }
        catch(AcaoInvalidaException expectedExcepttion){}
        
        try{
	        mesaTeste.setCapacidade(0);
	        fail("N�o deu exception!");
        }
        catch(AcaoInvalidaException expectedExcepttion){}
    }

    public void testIsLimpa() throws Exception {
        assertEquals(mesaTeste.isLimpa(), limpa);
    }

    public void testIsOcupada() throws Exception {
        assertEquals(mesaTeste.isOcupada(), ocupada);
    }

    public void testOcupa() throws Exception {
        mesaTeste.ocupa();
        assertEquals(mesaTeste.isOcupada(), new Boolean(true));
    }
    public void testJaOcupa() {
    	Boolean erro = false;
    	try {
    		limpa = false;
    		setUp();
			mesaTeste.ocupa();
			
		} catch (AcaoInvalidaException e) {
			erro = true;
		}    
    	catch(Exception e){
    		
    	}
        assertTrue(erro);
    }

    public void testDesocupa() throws Exception {
        mesaTeste.desocupa();
        assertEquals(mesaTeste.isOcupada(), new Boolean(false));
    }

    public void testSetReserva() throws Exception {
        Date dataTeste = new Date();
        mesaTeste.setReserva(dataTeste);
        assertEquals(mesaTeste.getReserva(), dataTeste );
        mesaTeste.setReserva(null);
        assertEquals(mesaTeste.getReserva(), null );
    }

    public void testGetReserva() throws Exception {
        assertEquals(mesaTeste.getReserva(), reserva );
    }

    public void testIsDisponivel() throws Exception {
        assertEquals(mesaTeste.isDisponivel(), new Boolean(false) );

        mesaTeste.setReserva(null);
        assertEquals(mesaTeste.isDisponivel(), new Boolean(true) );

        mesaTeste.ocupa();
        assertEquals(mesaTeste.isDisponivel(), new Boolean(false) );
    }

    public void testSetPedido() throws Exception {
        List<Item> itensTeste = new ArrayList<Item>();
        Estado estadoTeste = Estado.PENDENTE;
        Garcom garcomTeste = new Garcom();

        // create 2 itens
        Double preco = new Double(5.0f);
        String categoria = "Pães2";
        Double custo = new Double(2.00f);
        Double tempo = new Double(500);
        Receita receita;
        Item itemTeste;
        Ingrediente ingredienteTeste = new Ingrediente();
        Map<Ingrediente, Double> testeIngredientes = new HashMap();

        String teste_ingrediente_nome = "banana";
        String instrucoes_teste = "1-asse isso 2-asse aquilo";

        ingredienteTeste.setNome(teste_ingrediente_nome);
        testeIngredientes.put(ingredienteTeste, new Double(2.0f));
        receita = new Receita(instrucoes_teste, testeIngredientes );
        itemTeste = new Item(preco, categoria,custo,tempo,receita);
        teste_ingrediente_nome = "Farinha e fermento";
        instrucoes_teste = "1-asse isso 2-asse a";

        ingredienteTeste.setNome(teste_ingrediente_nome);
        testeIngredientes.put(ingredienteTeste, new Double(2.0f));
        receita = new Receita(instrucoes_teste, testeIngredientes );
        Item itemTeste2 = new Item(preco, categoria,custo,tempo,receita);

        itensTeste.add(itemTeste);
        itensTeste.add(itemTeste2);


        Pedido pedidoTeste2 = new Pedido(garcomTeste, itensTeste);
        mesaTeste.setPedido(pedidoTeste2);
        assertEquals(mesaTeste.getPedido(), pedidoTeste2 );

        mesaTeste.setPedido(null);
        assertEquals(mesaTeste.getPedido(), null );
    }

    public void testGetPedido() throws Exception {
        assertEquals(mesaTeste.getPedido(), pedidoTeste );
    }

    public void testSetGorjetaLimpeza() throws Exception {
        mesaTeste.setGorjetaLimpeza(new Double(5.00f));
        assertEquals(mesaTeste.getGorjetaLimpeza(), new Double(5.00f) );

        mesaTeste.setGorjetaLimpeza(new Double(-3.00f));
        assertNotSame(mesaTeste.getGorjetaLimpeza(), new Double(-3.00f) );
    }

    public void testGetGorjetaLimpeza() throws Exception {
        assertEquals(mesaTeste.getGorjetaLimpeza(), gorjetaLimpeza );
    }
}
package br.ufrgs.inf.engii.restaurante.test;

import br.ufrgs.inf.engii.restaurante.*;
import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class PedidoTest extends TestCase {
    Mesa mesaTeste;
    List<Item> itensTeste;
    Estado estadoTeste;
    Garcom garcomTeste;

    Pedido pedidoTeste;

    List<ItemPedido> itensPedidoTeste;
    public void setUp() throws Exception {
        super.setUp();

        mesaTeste = new Mesa();
        itensTeste = new ArrayList<Item>();
        estadoTeste = Estado.PENDENTE;
        garcomTeste = new Garcom();

        // create 2 itens
        Double preco = new Double(10.50f);
        String categoria = "categoria Teste";
        Double custo = new Double(5.00f);
        Double tempo = new Double(340);
        Receita receita;
        Item itemTeste;
        Ingrediente ingredienteTeste = new Ingrediente();
        Map<Ingrediente, Double> testeIngredientes = new HashMap();

        String teste_ingrediente_nome = "banana";
        String instrucoes_teste = "1-asse isso 2-asse aquilo";

        ingredienteTeste.setNome(teste_ingrediente_nome);
        testeIngredientes.put(ingredienteTeste, new Double(2.0f));
        receita = new Receita(instrucoes_teste, testeIngredientes );
        itemTeste = new Item(preco, categoria,custo,tempo,receita);
        teste_ingrediente_nome = "Canela";
        instrucoes_teste = "1-asse isso 2-asse aquilo 2.5 nao esqueca disso";

        ingredienteTeste.setNome(teste_ingrediente_nome);
        testeIngredientes.put(ingredienteTeste, new Double(2.0f));
        receita = new Receita(instrucoes_teste, testeIngredientes );
        Item itemTeste2 = new Item(preco, categoria,custo,tempo,receita);

        itensTeste.add(itemTeste);
        itensTeste.add(itemTeste2);

        itensPedidoTeste = new ArrayList<ItemPedido>();
        itensPedidoTeste.add(new ItemPedido(Estado.PENDENTE, itemTeste));
        itensPedidoTeste.add(new ItemPedido(Estado.PENDENTE, itemTeste2));

        pedidoTeste = new Pedido(garcomTeste, itensTeste);
        pedidoTeste.setMesa(mesaTeste);
    }

    public void testGetEstado() throws Exception {
        assertEquals(pedidoTeste.getEstado(), estadoTeste);
    }
    public void testCriaPedidoMutacao() throws Exception {
    	Pedido p = new Pedido(new Garcom(), new ArrayList<Item>());
        assertEquals(p.getEstado(), Estado.PENDENTE);
    }
    public void testAddItemPedidoMutacao() throws Exception {
    	Pedido p = new Pedido(new Garcom(), new ArrayList<Item>());
    	p.addItem(new Item(10.0, "Frio", 2.0, 20.0, new Receita()));
        assertEquals(p.getItens().get(0).getEstado(), Estado.PENDENTE);
    }
    public void testAddItemsPedidoMutacao() throws Exception {
    	ArrayList<Item> itens = new ArrayList<Item>();
    	itens.add(new Item(10.0, "Frio", 2.0, 20.0, new Receita()));
    	itens.add(new Item(20.0, "Quente", 4.0, 30.0, new Receita()));
    	Pedido p = new Pedido(new Garcom(), itens);
    	Boolean erro = false;
    	for (ItemPedido item : p.getItens()) {
			if(item.getEstado() != Estado.PENDENTE){
				erro = true;
				break;
			}
		}
        assertFalse(erro);
    }
    public void testGetCustoMutacao() throws Exception {
    	Pedido p = new Pedido(new Garcom(), new ArrayList<Item>());
    	p.addItem(new Item(10.0, "Frio", 2.0, 20.0, new Receita()));
        assertEquals(p.getCusto(), 2.0);
    }

    public void testSetEstado() throws Exception {
        pedidoTeste.setEstado(Estado.EM_PREPARACAO);
        assertEquals(pedidoTeste.getEstado(), Estado.EM_PREPARACAO);
        pedidoTeste.setEstado(Estado.PRONTO);
        assertEquals(pedidoTeste.getEstado(), Estado.PRONTO);
        pedidoTeste.setEstado(Estado.PENDENTE);
        assertEquals(pedidoTeste.getEstado(), Estado.PENDENTE);

    }

    public void testSetEstadoItens() throws Exception {

        Double preco = new Double(10.50f);
        String categoria = "categoria Teste";
        Double custo = new Double(5.00f);
        Double tempo = new Double(340);
        Receita receita;
        Item itemTeste;
        Ingrediente ingredienteTeste = new Ingrediente();
        Map<Ingrediente, Double> testeIngredientes = new HashMap();

        String teste_ingrediente_nome = "banana";
        String instrucoes_teste = "1-asse isso 2-asse aquilo";

        // create 2 itens
        ingredienteTeste.setNome(teste_ingrediente_nome);
        testeIngredientes.put(ingredienteTeste, new Double(2.0f));
        receita = new Receita(instrucoes_teste, testeIngredientes );
        itemTeste = new Item(preco, categoria,custo,tempo,receita);
        teste_ingrediente_nome = "Canela";
        instrucoes_teste = "1-asse isso 2-asse aquilo 2.5 nao esqueca disso";

        ingredienteTeste.setNome(teste_ingrediente_nome);
        testeIngredientes.put(ingredienteTeste, new Double(2.0f));
        receita = new Receita(instrucoes_teste, testeIngredientes );
        Item itemTeste2 = new Item(preco, categoria,custo,tempo,receita);

        itensTeste.add(itemTeste);
        itensTeste.add(itemTeste2);
        itensPedidoTeste.add(new ItemPedido(Estado.PENDENTE,itemTeste));
        itensPedidoTeste.add(new ItemPedido(Estado.PENDENTE,itemTeste2));

        pedidoTeste.setEstadoItens(itensTeste, Estado.PENDENTE);
        //System.out.println(pedidoTeste.getItens().get(2).getEstado());
        //System.out.println(itensPedidoTeste.get(2).getEstado());

        assertEquals(pedidoTeste.getItens().get(2).getEstado(), itensPedidoTeste.get(2).getEstado());
        assertEquals(pedidoTeste.getItens().get(2).getItem().getReceita().getInstrucoes(), itensPedidoTeste.get(2).getItem().getReceita().getInstrucoes());

    }

    public void testGetGarcom() throws Exception {
        assertEquals(pedidoTeste.getGarcom(), garcomTeste);
    }

    public void testSetGarcom() throws Exception {
        Garcom garcomTeste2 = new Garcom();
        pedidoTeste.setGarcom(garcomTeste2);
        assertEquals(pedidoTeste.getGarcom(), garcomTeste2);

    }

    public void testGetPreco() throws Exception {
        //System.out.print(pedidoTeste.getPreco());
        assertEquals(pedidoTeste.getPreco(), new Double(10.50f+10.50f));
    }

    public void testAddItems() throws Exception {
        Double teste = pedidoTeste.getCusto()*2;
        pedidoTeste.addItems(pedidoTeste);
        assertEquals(pedidoTeste.getCusto(), teste);
    }

    public void testAddItem() throws Exception {
        Double preco = new Double(10.50f);
        String categoria = "categoria Teste";
        Double custo = new Double(5.00f);
        Double tempo = new Double(340);
        Receita receita;
        Item itemTeste;
        Ingrediente ingredienteTeste = new Ingrediente();
        Map<Ingrediente, Double> testeIngredientes = new HashMap();

        String teste_ingrediente_nome = "banana";
        String instrucoes_teste = "1-asse isso 2-asse aquilo";

        ingredienteTeste.setNome(teste_ingrediente_nome);
        testeIngredientes.put(ingredienteTeste, new Double(2.0f));
        receita = new Receita(instrucoes_teste, testeIngredientes );
        itemTeste = new Item(preco, categoria,custo,tempo,receita);
        itensTeste.add(itemTeste);

        pedidoTeste.addItem(itemTeste);
        itensPedidoTeste.add(new ItemPedido(Estado.PENDENTE, itemTeste));

        assertEquals(pedidoTeste.getItens().get(2).getItem().getReceita().getInstrucoes(), itensPedidoTeste.get(2).getItem().getReceita().getInstrucoes());
    }

    public void testGetItens() throws Exception {
        assertEquals(pedidoTeste.getItens().get(1).getItem().getCusto(), itensTeste.get(1).getCusto());

    }

    public void testGetMesa() throws Exception {
        assertEquals(pedidoTeste.getMesa().getSetor(), mesaTeste.getSetor());
    }

    public void testSetMesa() throws Exception {
        Mesa mesaTeste2 = new Mesa();
        pedidoTeste.setMesa(mesaTeste2);

        assertEquals(pedidoTeste.getMesa(), mesaTeste2);

    }

    public void testGetTaxaServico() throws Exception {
        assertEquals(pedidoTeste.getTaxaServico(), pedidoTeste.getPreco()*0.3);
    }

}
package br.ufrgs.inf.engii.restaurante.test;

import br.ufrgs.inf.engii.restaurante.*;
import junit.framework.TestCase;

import java.util.HashMap;
import java.util.Map;


public class ReceitaTest extends TestCase {
    String teste_ingrediente_nome = "banana";
    String teste_ingrediente_nome2 = "manteiga";
    String instrucoes_teste = "1-asse isso 2-asse aquilo";
    String instrucoes_teste2 = "1-asse isso 2-asse aquilo 3-nao esqueca daquilo tambem";
    Ingrediente ingredienteTeste = new Ingrediente();
    Receita receitaTeste;
    Map<Ingrediente, Double> testeIngredientes = new HashMap();


    public void setUp() throws Exception {
        super.setUp();
        ingredienteTeste.setNome(teste_ingrediente_nome);
        testeIngredientes.put(ingredienteTeste, new Double(2.0f));
        receitaTeste = new Receita();
        receitaTeste = new Receita(instrucoes_teste, testeIngredientes );

    }

    public void tearDown() throws Exception {

    }

    public void testGetInstrucoes() throws Exception {
        assertEquals(receitaTeste.getInstrucoes(), instrucoes_teste);
    }

    public void testSetInstrucoes() throws Exception {
        receitaTeste.setInstrucoes(instrucoes_teste2);
        assertEquals(receitaTeste.getInstrucoes(), instrucoes_teste2);
    }

    public void testGetIngredientes() throws Exception {
        assertEquals(receitaTeste.getIngredientes(), testeIngredientes);
    }

    public void testSetIngredientes() throws Exception {
        ingredienteTeste.setNome("Manteiga");
        testeIngredientes.put(ingredienteTeste, new Double(6.0f));
        receitaTeste.setIngredientes(testeIngredientes);
        assertEquals(receitaTeste.getIngredientes(), testeIngredientes);
    }

}
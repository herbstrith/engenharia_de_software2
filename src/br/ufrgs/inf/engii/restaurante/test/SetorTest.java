package br.ufrgs.inf.engii.restaurante.test;

import br.ufrgs.inf.engii.restaurante.*;
import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;


public class SetorTest extends TestCase {
    Setor setorTeste;
    List<Mesa> mesasTeste;
    public void setUp() throws Exception {
        super.setUp();
        mesasTeste = new ArrayList<Mesa>();
        setorTeste = new Setor();
        mesasTeste.add(new Mesa());
        mesasTeste.add(new Mesa());
        setorTeste.setMesas(mesasTeste);


    }

    public void tearDown() throws Exception {

    }

    public void testGetMesas() throws Exception {
        assertEquals(setorTeste.getMesas(), mesasTeste);
    }

    public void testSetMesas() throws Exception {
        List<Mesa> mesasTeste2 = new ArrayList<Mesa>();
        mesasTeste2.add(new Mesa());
        mesasTeste2.add(new Mesa());

        setorTeste.setMesas(mesasTeste2);
        assertEquals(setorTeste.getMesas(), mesasTeste2);

    }

}
package br.ufrgs.inf.engii.restaurante.test;

import br.ufrgs.inf.engii.restaurante.*;
import br.ufrgs.inf.engii.restaurante.exceptions.PedidoEmAbertoException;
import br.ufrgs.inf.engii.restaurante.exceptions.PedidoFechadoException;
import br.ufrgs.inf.engii.restaurante.exceptions.PedidoInexistenteException;
import junit.framework.TestCase;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.channels.GatheringByteChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Herbstrith on 10/05/2016.
 */
public class TurnoTest extends TestCase {


    Double custoTeste;
    Double lucroTeste;
    Map<Garcom, Setor> garcomSetorTeste;
    List<Pedido> pedidosTeste;
    Map<Funcionario, Double> gorjetasTeste;

    Turno turnoTeste;
    Pedido pedidoTeste;
    Garcom garcomTeste;
    Garcom garcomTeste2;
    Setor setorTeste;

    Map<List<Garcom>,Setor> garconsSetorTeste;

    public void setUp() throws Exception {
        super.setUp();
        custoTeste = new Double(10);
        lucroTeste = new Double(8);

        turnoTeste = new Turno();
        List<Item> itensTeste = new ArrayList<Item>();
        Estado estadoTeste = Estado.PENDENTE;
        garcomTeste = new Garcom();
        garcomTeste.setValorFixo(1000.00);
        garcomTeste2 = new Garcom();
        garcomTeste2.setValorFixo(1500.00);
        setorTeste = new Setor();

        List<Garcom> listGarcom = new ArrayList<Garcom>();
        listGarcom.add(garcomTeste);
        listGarcom.add(garcomTeste2);
        garconsSetorTeste = new HashMap();
        garconsSetorTeste.put(listGarcom, setorTeste);

        // create 2 itens
        Double preco = new Double(10.50f);
        String categoria = "categoria Teste";
        Double custo = new Double(5.00f);
        Double tempo = new Double(340);
        Receita receita;
        Item itemTeste;
        Ingrediente ingredienteTeste = new Ingrediente();
        Map<Ingrediente, Double> testeIngredientes = new HashMap();

        String teste_ingrediente_nome = "banana";
        String instrucoes_teste = "1-asse isso 2-asse aquilo";

        ingredienteTeste.setNome(teste_ingrediente_nome);
        testeIngredientes.put(ingredienteTeste, new Double(2.0f));
        receita = new Receita(instrucoes_teste, testeIngredientes );
        itemTeste = new Item(preco, categoria,custo,tempo,receita);
        teste_ingrediente_nome = "Canela";
        instrucoes_teste = "1-asse isso 2-asse aquilo 2.5 nao esqueca disso";

        ingredienteTeste.setNome(teste_ingrediente_nome);
        testeIngredientes.put(ingredienteTeste, new Double(2.0f));
        receita = new Receita(instrucoes_teste, testeIngredientes );
        Item itemTeste2 = new Item(preco, categoria,custo,tempo,receita);

        itensTeste.add(itemTeste);
        itensTeste.add(itemTeste2);



        pedidoTeste = new Pedido(garcomTeste, itensTeste);
        pedidoTeste.setMesa(new Mesa());
    }

    public void testAddPedido() throws Exception {
        turnoTeste.addPedido(pedidoTeste);

        List<Item> itensTeste = new ArrayList<Item>();
        Estado estadoTeste = Estado.PENDENTE;
        Garcom garcomTeste = new Garcom();
        // create 2 itens
        Double preco = new Double(15.50f);
        String categoria = "categoria Teste";
        Double custo = new Double(8.00f);
        Double tempo = new Double(200);
        Receita receita;
        Item itemTeste;
        Ingrediente ingredienteTeste = new Ingrediente();
        Map<Ingrediente, Double> testeIngredientes = new HashMap();

        String teste_ingrediente_nome = "banana";
        String instrucoes_teste = "1-asse isso 2-asse aquilo";

        ingredienteTeste.setNome(teste_ingrediente_nome);
        testeIngredientes.put(ingredienteTeste, new Double(2.0f));
        receita = new Receita(instrucoes_teste, testeIngredientes );
        itemTeste = new Item(preco, categoria,custo,tempo,receita);
        teste_ingrediente_nome = "Canela";
        instrucoes_teste = "1-asse isso 2-asse aquilo";

        ingredienteTeste.setNome(teste_ingrediente_nome);
        testeIngredientes.put(ingredienteTeste, new Double(2.0f));
        receita = new Receita(instrucoes_teste, testeIngredientes );
        Item itemTeste2 = new Item(preco, categoria,custo,tempo,receita);

        itensTeste.add(itemTeste);
        itensTeste.add(itemTeste2);



        Pedido pedidoTeste2 = new Pedido(garcomTeste, itensTeste);
        turnoTeste.addPedido(pedidoTeste2);

        assertEquals(turnoTeste.getPedidos().get(0), pedidoTeste);
        assertEquals(turnoTeste.getPedidos().get(1), pedidoTeste2);

    }

    public void testGetPedidos() throws Exception {
        turnoTeste.addPedido(pedidoTeste);
        assertEquals(turnoTeste.getPedidos().get(0), pedidoTeste);
    }

    public void testGetSetor() throws Exception {
        turnoTeste.setGarconsSetor(garconsSetorTeste);
        assertEquals(turnoTeste.getSetor(garcomTeste), setorTeste);
    }

    public void testAddGorjeta() throws Exception {
        turnoTeste.setGarconsSetor(garconsSetorTeste);
        turnoTeste.addGorjeta(garcomTeste, new Double(10f));
        assertEquals(turnoTeste.getFolhaPgto().get(garcomTeste), 10.0+garcomTeste.getValorFixo());
        turnoTeste.addGorjeta(garcomTeste2, new Double(22));
        assertEquals(turnoTeste.getFolhaPgto().get(garcomTeste2), 22.0+garcomTeste2.getValorFixo());

    }

    public void testGetGarcons() throws Exception {
        turnoTeste.setGarconsSetor(garconsSetorTeste);
        assertTrue(turnoTeste.getGarcons().contains(garcomTeste));

    }

    public void testSetGarconsSetor() throws Exception {
        turnoTeste.setGarconsSetor(garconsSetorTeste);
        assertEquals(turnoTeste.getSetor(garcomTeste), setorTeste);

    }

    public void testGetSomaLucro() throws Exception {
    	Double lucroNovo = turnoTeste.getSomaLucro() + pedidoTeste.getPreco()-pedidoTeste.getCusto() - pedidoTeste.getTaxaServico();
    	turnoTeste.finalizarPedido(pedidoTeste, false);
    	BigDecimal bdln = new BigDecimal(lucroNovo).setScale(2, RoundingMode.HALF_EVEN);
    	BigDecimal bdsl = new BigDecimal(turnoTeste.getSomaLucro()).setScale(2, RoundingMode.HALF_EVEN);
        assertEquals(bdsl, bdln);
    }
    
    public void testGetSomaCusto() throws Exception {
    	Double CustoNovo = turnoTeste.getSomaCustos() + pedidoTeste.getCusto() + pedidoTeste.getTaxaServico();
    	turnoTeste.finalizarPedido(pedidoTeste, false);
    	BigDecimal bdln = new BigDecimal(CustoNovo).setScale(2, RoundingMode.HALF_EVEN);
    	BigDecimal bdsl = new BigDecimal(turnoTeste.getSomaCustos()).setScale(2, RoundingMode.HALF_EVEN);
        assertEquals(bdsl, bdln);
    }
    
    public void testGetSomaLucroComServico() throws Exception {
    	Double lucroNovo = turnoTeste.getSomaLucro() + pedidoTeste.getPreco()-pedidoTeste.getCusto();
    	turnoTeste.finalizarPedido(pedidoTeste, true);
    	BigDecimal bdln = new BigDecimal(lucroNovo).setScale(2, RoundingMode.HALF_EVEN);
    	BigDecimal bdsl = new BigDecimal(turnoTeste.getSomaLucro()).setScale(2, RoundingMode.HALF_EVEN);
        assertEquals(bdsl, bdln);
    }

    public void testAlterarEstadoItensFinalizado() throws Exception {
        turnoTeste.addPedido(pedidoTeste);
        turnoTeste.alterarEstadoItensFinalizado(pedidoTeste);
        assertEquals(turnoTeste.getPedidos().get(turnoTeste.getPedidos().size()-1).getItens().get(0).getEstado(), Estado.PRONTO);
    }

    public void testTodosItensFinalizado() throws Exception {
        turnoTeste.addPedido(pedidoTeste);
        turnoTeste.finalizarPedido(pedidoTeste, false);    
        assertEquals(turnoTeste.todosItensFinalizado(pedidoTeste), new Boolean(true));
    }
    public void testTodosItensFinalizadoMutacao() throws Exception {
    	ArrayList<Item> itens = new ArrayList<Item>();
    	itens.add(new Item(10.0, "Frio", 2.0, 20.0, new Receita()));
    	itens.add(new Item(20.0, "Quente", 4.0, 30.0, new Receita()));
    	Pedido p = new Pedido(new Garcom(), itens);
    	Turno t = new Turno();
    	t.addPedido(p);     	
        assertFalse(t.todosItensFinalizado(p));
    }

    public void testFinalizarPedido() throws Exception {
        turnoTeste.finalizarPedido(pedidoTeste, new Boolean(false));
        assertEquals(pedidoTeste.getEstado(), Estado.PRONTO);
    }
    public void testFinalizarPedidoNullsMutacao() throws Exception {
        turnoTeste.finalizarPedido(pedidoTeste, new Boolean(true));
        Mesa m =  pedidoTeste.getMesa();
        assertFalse(m.isOcupada());
        assertTrue(m.getGorjetaLimpeza() > 0);
    }
    public void testFinalizarPedidoMultMutacao() throws Exception {
        turnoTeste.finalizarPedido(pedidoTeste, new Boolean(true));
        assertTrue(turnoTeste.getFolhaPgto().get(pedidoTeste.getGarcom()) > pedidoTeste.getTaxaServico());
        assertTrue(pedidoTeste.getMesa().getGorjetaLimpeza() < pedidoTeste.getTaxaServico());
    }

    public void testGetFolhaPgto() throws Exception {
        turnoTeste.setGarconsSetor(garconsSetorTeste);
        turnoTeste.addGorjeta(garcomTeste, new Double(10f));
        assertEquals(turnoTeste.getFolhaPgto().get(garcomTeste), new Double(10f)+garcomTeste.getValorFixo());
    }
    
    public void testFinalizaPedidoInexistente(){
        Boolean erro = false;
    	try {
			Pedido p =new Pedido(garcomTeste, new ArrayList<Item>());
			turnoTeste.alterarEstadoItensFinalizado(p);
		} catch (PedidoInexistenteException e) {
			erro = true;
		}
        assertTrue(erro);
    }
    
    public void testFolhaMutacao() throws PedidoEmAbertoException, PedidoFechadoException {
    	turnoTeste.addPedido(pedidoTeste);
        turnoTeste.finalizarPedido(pedidoTeste, false);    
        Map<Funcionario, Double> folha = turnoTeste.getFolhaPgto();
        Boolean falha = false;
        for (Funcionario f : folha.keySet()) {
			if(folha.get(f) > turnoTeste.getSomaLucro()){
				falha = true;
				break;
			}
		}
    	assertFalse(falha);
    }
    
    public void testFolhaSomaMutacao() throws PedidoEmAbertoException, PedidoFechadoException {
    	turnoTeste.addPedido(pedidoTeste);
        turnoTeste.finalizarPedido(pedidoTeste, false);    
        Map<Funcionario, Double> folha = turnoTeste.getFolhaPgto();
        Boolean falha = false;
        for (Funcionario f : folha.keySet()) {
			if(f.getValorFixo() > folha.get(f) ){
				falha = true;
				break;
			}
		}
    	assertFalse(falha);
    }
    public void testFolhaTaxaSMutacao() throws PedidoEmAbertoException, PedidoFechadoException {
    	ArrayList<Item> itens = new ArrayList<Item>();
    	itens.add(new Item(10.0, "Frio", 2.0, 20.0, new Receita()));
    	itens.add(new Item(20.0, "Quente", 4.0, 30.0, new Receita()));
    	Garcom f = new Garcom();
    	Pedido p = new Pedido(f, itens);
    	p.setMesa(new Mesa());
    	Turno t = new Turno();
    	t.addPedido(p);
    	t.finalizarPedido(p, true);
        Map<Funcionario, Double> folha = t.getFolhaPgto();
        
        BigDecimal bdln = new BigDecimal(folha.get(f)).setScale(2, RoundingMode.HALF_EVEN);
    	BigDecimal bdsl = new BigDecimal(f.getValorFixo()+p.getTaxaServico()).setScale(2, RoundingMode.HALF_EVEN);
        assertTrue( bdln.doubleValue() <= bdsl.doubleValue());
        
    }

}